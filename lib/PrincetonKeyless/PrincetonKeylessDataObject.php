<?php

class PrincetonKeylessDataObject extends KGODataObject
{
    const KEYLESS_DESCRIPTION_ATTRIBUTE = 'pu:description';
    const NAME_ATTRIBUTE = 'pu:name';
    const BUILDING_ID_ATTRIBUTE = 'pu:building_id';
    const BUILDING_NAME_ATTRIBUTE = 'pu:building_name';

    public function getkeylessname() {
        return $this->getAttribute(self::NAME_ATTRIBUTE);
    }

    public function getDescription() {
        return $this->getAttribute(self::KEYLESS_DESCRIPTION_ATTRIBUTE);
    }
    
    public function get_building_id() {
        return $this->getAttribute(self::BUILDING_ID_ATTRIBUTE);
    }

    public function get_building_name() {
        return $this->getAttribute(self::BUILDING_NAME_ATTRIBUTE);
    }

    public function getUIField(KGOUIObject $object, $field) {
        //kgo_debug($field,false,false);       
        switch ($field) {     
            case 'title':
                $name=$this->getkeylessname();
                return $name;
            case 'subtitle':
                $description=$this->getDescription();
                $building_name=$this->get_building_name();
                $building_id=$this->get_building_id();
                $tmp_array = array($description,"$building_name ($building_id)");
                $tmp_info=implode("</br >",$tmp_array);
                return $tmp_info;
            default:
                return parent::getUIField($object, $field);
        }
    }
}
